package farhat.restapi.util;

public final class RestUtils {

    public static final String API_PREFIX = "/api/" ;

    public static final String API_USERS = "users";

    /**
     * <p>
     * Suppress default constructor for noninstantiability.
     * </p>
     */
    private RestUtils() {
        throw new AssertionError();
    }
}
