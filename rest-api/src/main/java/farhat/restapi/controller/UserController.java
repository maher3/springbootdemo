package farhat.restapi.controller;

import farhat.api.dto.UserDto;
import farhat.api.service.user.IUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import static farhat.restapi.util.RestUtils.API_PREFIX;
import static farhat.restapi.util.RestUtils.API_USERS;

@RestController
@RequestMapping(value = API_PREFIX + API_USERS)
@Api(value= API_USERS , description = "end point for users managements")
public class UserController {

    @Autowired
    private IUserService userService;

    @GetMapping
    @ApiOperation(value ="get all users ")
    public List<UserDto> allUser() {

        return userService.findAll();
    }
    @PostMapping
    public UserDto saveUser(@RequestBody UserDto userDto){
        return userService.Save(userDto);
    }
}
