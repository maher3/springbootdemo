package farhat.api.service.user;

import farhat.api.dto.UserDto;

import java.util.List;

public interface IUserService {

    List<UserDto> findAll();
    UserDto         Save(UserDto userDto);

}
