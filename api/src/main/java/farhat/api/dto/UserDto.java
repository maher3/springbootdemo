package farhat.api.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = {"id"})
@ToString(of = {"id", "lastName", "lastName"})
public class UserDto {

    private Long id;
    private String firstname;
}
