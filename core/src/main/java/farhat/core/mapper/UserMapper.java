package farhat.core.mapper;

import farhat.api.dto.UserDto;
import farhat.core.entity.User;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper {

    private static final ModelMapper MAPPER = new ModelMapper();

    public List<UserDto> mapEntitiesIntoDtos(List<User> entities) {
        return entities.stream().map(this::mapEntityIntoDto).collect(Collectors.toList());
    }

    public UserDto mapEntityIntoDto(User entity) {

        final UserDto userDto = MAPPER.map(entity, UserDto.class);

        return userDto;
    }

    public User mapDtoIntoEntity(UserDto userDto){
        final User user = MAPPER.map(userDto, User.class);
        return user ;
    }


}
