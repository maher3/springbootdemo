package farhat.core.services.user;


import farhat.api.dto.UserDto;
import farhat.api.service.user.IUserService;
import farhat.core.entity.User;
import farhat.core.mapper.UserMapper;
import farhat.core.repository.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService implements IUserService {

    private UserRepository userRepository;
    private UserMapper userMapper;

    public UserService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    @Override
    public List<UserDto> findAll() {
        List<User> users = userRepository.findAll();
        return userMapper.mapEntitiesIntoDtos(users);
    }


    @Override
    @Transactional(readOnly = false)
     public UserDto Save(UserDto userDto) {
          User user = userMapper.mapDtoIntoEntity(userDto);
          userRepository.save(user);
    return userDto ;
    }

}
